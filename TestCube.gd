extends RigidBody

var SPEED_PJ = 10
var SPEED_ATACK = 15
var MAX_OFFSET = 3

var atack = false

var cube
var sword

func _ready():
	set_fixed_process(true)
	set_process_input(true)
	cube = get_node("PersonajeForma")
	sword = get_node("PersonajeForma/PathAtaque/PathFollowEspada")
	sword.get_node("EspadaGrafico/Espada").connect("body_enter", self, "_on_Espada_body_enter")
	get_node("Camera").make_current()

func _input(event):
	if event.type == InputEvent.MOUSE_BUTTON:
		if event.button_index == 1:
			if event.is_pressed():
				atack = true

func _fixed_process(delta):
	if atack:
		var offset = sword.get_offset()
		sword.set_offset(offset + SPEED_ATACK*delta)
		if offset>MAX_OFFSET:
			sword.set_offset(0)
			atack = false
	
	if Input.is_key_pressed( KEY_W ):
		global_translate( Vector3(0,0,-SPEED_PJ*delta) )
	if Input.is_key_pressed( KEY_A ):
		global_translate( Vector3(-SPEED_PJ*delta,0,0) )
	if Input.is_key_pressed( KEY_D ):
		global_translate( Vector3(SPEED_PJ*delta,0,0) )
	if Input.is_key_pressed( KEY_S ):
		global_translate( Vector3(0,0,SPEED_PJ*delta) )
	
	var screen = get_viewport().get_rect()
	var mousePos = get_viewport().get_mouse_pos()
	var cordX = (mousePos.x - screen.size.x/2)/(screen.size.x)*2
	var cordY = (mousePos.y - screen.size.y/2)/(screen.size.y)*2
	var angle = atan2( cordX , cordY)
	cube.set_rotation(Vector3(0,angle+PI,0))
	
func _on_body_enter(body):
	print("colisionó con: ", body)
	pass

func _on_Espada_body_enter( body ):
	if atack:
		print("hizo daño a: ", body)
	pass
