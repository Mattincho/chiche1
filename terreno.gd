extends Node

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	var pj = load("res://TestCube.tscn").instance()
	add_child(pj)
	pj.connect("body_enter", pj, "_on_body_enter")
	pass

